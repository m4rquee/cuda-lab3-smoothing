#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define MASK_WIDTH 5 // Odd number
#define RADIUS 2 // (MASK_WIDTH - 1) / 2

#define RGB_COMPONENT_COLOR 255

#define T 32

typedef struct {
  unsigned char red, green, blue;
} PPMPixel;

typedef struct {
  int x, y;
  PPMPixel *data;
} PPMImage;

double rtclock() {
  struct timezone Tzp;
  struct timeval Tp;
  int stat;
  stat = gettimeofday(&Tp, &Tzp);
  if (stat != 0)
    printf("Error return from gettimeofday: %d", stat);
  return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
}

static PPMImage *readPPM(const char *filename) {
  char buff[16];
  PPMImage *img;
  FILE *fp;
  int c, rgb_comp_color;
  fp = fopen(filename, "rb");
  if (!fp) {
    fprintf(stderr, "Unable to open file '%s'\n", filename);
    exit(1);
  }

  if (!fgets(buff, sizeof(buff), fp)) {
    perror(filename);
    exit(1);
  }

  if (buff[0] != 'P' || buff[1] != '6') {
    fprintf(stderr, "Invalid image format (must be 'P6')\n");
    exit(1);
  }

  img = (PPMImage *)malloc(sizeof(PPMImage));
  if (!img) {
    fprintf(stderr, "Unable to allocate memory\n");
    exit(1);
  }

  c = getc(fp);
  while (c == '#') {
    while (getc(fp) != '\n')
      ;

    c = getc(fp);
  }

  ungetc(c, fp);
  if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
    fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
    exit(1);
  }

  if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
    fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
    exit(1);
  }

  if (rgb_comp_color != RGB_COMPONENT_COLOR) {
    fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
    exit(1);
  }

  while (fgetc(fp) != '\n')
    ;

  img->data = (PPMPixel *)malloc(img->x * img->y * sizeof(PPMPixel));

  if (!img) {
    fprintf(stderr, "Unable to allocate memory\n");
    exit(1);
  }

  if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
    fprintf(stderr, "Error loading image '%s'\n", filename);
    exit(1);
  }

  fclose(fp);
  return img;
}

void writePPM(PPMImage *img) {
  fprintf(stdout, "P6\n");
  fprintf(stdout, "%d %d\n", img->x, img->y);
  fprintf(stdout, "%d\n", RGB_COMPONENT_COLOR);

  fwrite(img->data, 3 * img->x, img->y, stdout);
  fclose(stdout);
}

int *allocvet(int size) { return (int *)malloc(size * sizeof(int)); }

void image_channels(PPMImage *image, int **red, int **green, int **blue,
                    int size) {
  *red = allocvet(size);
  *green = allocvet(size);
  *blue = allocvet(size);

  for (int i = 0; i < size; i++) {
    (*red)[i] = image->data[i].red;
    (*green)[i] = image->data[i].green;
    (*blue)[i] = image->data[i].blue;
  }
}

void image_from_channels(int *red, int *green, int *blue, PPMImage *image,
                         int size) {
  for (int i = 0; i < size; i++) {
    image->data[i].red = red[i];
    image->data[i].green = green[i];
    image->data[i].blue = blue[i];
  }
}

__global__ void smoothing_GPUX(int *channel, int *channel_out, int N, int M) {
  int lrow = threadIdx.y;
  int col = T * blockIdx.x + threadIdx.x;
  int row = T * blockIdx.y + lrow;

  if (row < N && col < M)
    return;

  __shared__ int temp[T][MASK_WIDTH - 1 + T];

  int gindex = row * M + col;
  int lindex = threadIdx.x + RADIUS;

  // Copy to shared:
  temp[lrow][lindex] = channel[gindex];
  if (threadIdx.x < RADIUS) {
    if (col >= RADIUS)
      temp[lrow][lindex - RADIUS] = channel[gindex - RADIUS];
    else
      temp[lrow][lindex - RADIUS] = 0;
  } else if (threadIdx.x >= blockDim.x - RADIUS) {
    if (col < M - RADIUS)
      temp[lrow][lindex + RADIUS] = channel[gindex + RADIUS];
    else
      temp[lrow][lindex + RADIUS] = 0;
  }

  __syncthreads();

  // Apply the stencil:
  int result = 0;
  for (int off = -RADIUS; off <= RADIUS; off++)
    result += temp[lrow][lindex + off];

  // Store the result:
  channel_out[gindex] = result;
}

__global__ void smoothing_GPUY(int *channel, int *channel_out, int N, int M) {
  int lcol = threadIdx.x;
  int col = T * blockIdx.x + lcol;
  int row = T * blockIdx.y + threadIdx.y;

  if (row < N && col < M)
    return;

  __shared__ int temp[T][MASK_WIDTH - 1 + T]; // Transposed

  int gindex = row * M + col;
  int lindex = threadIdx.y + RADIUS; // Transposed

  // Copy to shared:
  temp[lcol][lindex] = channel[gindex];
  if (threadIdx.y < RADIUS) {
    if (row >= RADIUS)
      temp[lcol][lindex - RADIUS] = channel[gindex - RADIUS];
    else
      temp[lcol][lindex - RADIUS] = 0;
  } else if (threadIdx.y >= blockDim.y - RADIUS) {
    if (row < N - RADIUS)
      temp[lcol][lindex + RADIUS] = channel[gindex + RADIUS];
    else
      temp[lcol][lindex + RADIUS] = 0;
  }

  __syncthreads();

  // Apply the stencil:
  int result = 0;
  for (int off = -RADIUS; off <= RADIUS; off++)
    result += temp[lcol][lindex + off];

  // Store the result:
  channel_out[gindex] = result / (MASK_WIDTH * MASK_WIDTH);
}

void smoothing(int *red, int *green, int *blue, int x, int y) {
  int size = x * y * sizeof(int);
  int *d_red, *d_green, *d_blue, *d_red_aux, *d_green_aux, *d_blue_aux;

  cudaStream_t s1, s2, s3;
  cudaStreamCreate(&s1);
  cudaStreamCreate(&s2);
  cudaStreamCreate(&s3);

  cudaMalloc(&d_red, size);
  cudaMalloc(&d_green, size);
  cudaMalloc(&d_blue, size);
  cudaMalloc(&d_red_aux, size);
  cudaMalloc(&d_green_aux, size);
  cudaMalloc(&d_blue_aux, size);

  dim3 dimGrid(ceil((float) x / T), ceil((float) y / T), 1);
  dim3 dimBlock(T, T, 1);

  // Smoothing_GPUX:
  cudaMemcpyAsync(d_red, red, size, cudaMemcpyHostToDevice, s1);
  smoothing_GPUX<<<dimGrid, dimBlock, 0, s1>>>(d_red, d_red_aux, y, x);

  cudaMemcpyAsync(d_green, green, size, cudaMemcpyHostToDevice, s2);
  smoothing_GPUX<<<dimGrid, dimBlock, 0, s2>>>(d_green, d_green_aux, y, x);

  cudaMemcpyAsync(d_blue, blue, size, cudaMemcpyHostToDevice, s3);
  smoothing_GPUX<<<dimGrid, dimBlock, 0, s3>>>(d_blue, d_blue_aux, y, x);

  // Smoothing_GPUY:
  smoothing_GPUY<<<dimGrid, dimBlock, 0, s1>>>(d_red_aux, d_red, y, x);
  cudaMemcpyAsync(red, d_red, size, cudaMemcpyDeviceToHost, s1);

  smoothing_GPUY<<<dimGrid, dimBlock, 0, s2>>>(d_green_aux, d_green, y, x);
  cudaMemcpyAsync(green, d_green, size, cudaMemcpyDeviceToHost, s2);
  
  smoothing_GPUY<<<dimGrid, dimBlock, 0, s3>>>(d_blue_aux, d_blue, y, x);
  cudaMemcpyAsync(blue, d_blue, size, cudaMemcpyDeviceToHost, s3);  

  cudaStreamDestroy(s1);
  cudaStreamDestroy(s2);
  cudaStreamDestroy(s3);

  cudaFree(d_red);
  cudaFree(d_green);
  cudaFree(d_blue);
  cudaFree(d_red_aux);
  cudaFree(d_green_aux);
  cudaFree(d_blue_aux);
}

int main(int argc, char *argv[]) {
  int *red, *green, *blue;

  if (argc != 2)
    printf("Too many or no one arguments supplied.\n");

  // Read image:
  PPMImage *image = readPPM("../inputs/arq3.ppm");

  // Channels:
  int size = image->x * image->y;
  image_channels(image, &red, &green, &blue, size);

  // Smoothing:
  smoothing(red, green, blue, image->x, image->y);

  // Write image:
  image_from_channels(red, green, blue, image, size);
  writePPM(image);

  free(image->data);
  free(image);
  free(red);
  free(green);
  free(blue);

  return 0;
}
