cmake_minimum_required(VERSION 2.8)
project(smoothing)

find_package(CUDA REQUIRED)

add_executable(smooth src/smooth_serial.c)
target_link_libraries(smooth m)

#cuda_add_executable(smooth-gpu src/smooth_gpu.cu)
